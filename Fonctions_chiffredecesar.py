
def dechiffre_lettre(lettre, decalage):
    codeascii = ord(lettre)
    codeascii = codeascii - decalage   #On modifie le code ascii pour obtenir celui de la lettre déchiffrée
    lettre = chr(codeascii)    #On déchiffre la lettre avec le nouveau code ascii
    return lettre   #On returne la lettre décryptée

def chiffre_lettre(lettre, decalage):
    codeascii = ord(lettre)
    codeascii = codeascii + decalage   #On modifie le code ascii pour obtenir celui de la lettre chiffrée
    lettre = chr(codeascii)  #On chiffre la lettre avec le nouveau code ascii
    return lettre   #On returne la lettre cryptée

def dechiffrage_mots(mot):
    decalage=int(input("entrez votre décalage: "))
    message_decrypte = []
    for lettre in mot:  
        if lettre!=' ':
            lettre_decrypte = dechiffre_lettre(lettre, decalage)    #On dechiffre chaque lettre une par une 
            message_decrypte.append(lettre_decrypte)    #On met toutes les lettres déchifrées dans notre liste
        else:
            message_decrypte.append(lettre)
    return(''.join(message_decrypte))      #On returne le message déchiffré

def chiffrage_mots(mot):
    decalage=int(input("entrez votre décalage: "))
    message_crypte = []
    for lettre in mot: 
        if lettre!=' ': 
            lettre_crypte = chiffre_lettre(lettre, decalage)    #On crypte chaque lettre une par une 
            message_crypte.append(lettre_crypte)    #On met toutes les lettres chifrées dans une seule chaine de carctères
        else:
            message_crypte.append(lettre)
    return(''.join(message_crypte))      #On returne le message chiffré
