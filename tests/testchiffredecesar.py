from Fonctions_chiffredecesar import *
import unittest

class Testchiffre_de_CesarFunctions(unittest.TestCase):

    def test_dechiffre_lettre(self):
        self.assertEqual(dechiffre_lettre('t', 13), 'g')
        self.assertEqual(dechiffre_lettre('z', 13), 'm')
        self.assertEqual(dechiffre_lettre('N', 13), 'A')

    def test_chiffre_lettre(self):
        self.assertEqual(chiffre_lettre('c', 13), 'p')
        self.assertEqual(chiffre_lettre('m', 13), 'z')
        self.assertEqual(chiffre_lettre('b', 13), 'o')

    def test_dechiffrage_mot(self):
        self.assertEqual(dechiffrage_mots("nop"), "abc")
        self.assertEqual(dechiffrage_mots("o|{w|\x82\x7f"), "bonjour")
        self.assertEqual(dechiffrage_mots("L M N"), "? @ A")

    def test_chiffrage_mot(self):
        self.assertEqual(chiffrage_mots("abc"), "nop")
        self.assertEqual(chiffrage_mots("bonjour"), "o|{w|\x82\x7f")
        self.assertEqual(chiffrage_mots("D EF"), "Q RS")