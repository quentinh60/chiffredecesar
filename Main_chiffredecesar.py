from Fonctions_chiffredecesar import *
from tkinter import * 
from tkinter.messagebox import *

color_grey = "#7f8c8d"
color_lightgrey = "#95a5a6"
def get_textarea_content():
    ''' 
    Fonction qui nous renvoi le texte de notre textarea
    '''
    return textarea.get("0.0", END).strip()

def verification_textarea_content():
    '''
    On vérifie que textarea n'est pas vide
    '''
    txt = get_textarea_content()
    if len(txt) == 0:
        showwarning(title="yarien", message="Entrez du texte !")
        return False
    return True

def dechiffrage_phrase():
    '''
    Ici on appelle la fonction de dechiffrage de notre fichier Fonctions_chiffredecesar
    '''
    if verification_textarea_content():
        txt = get_textarea_content()
        showinfo(title="decryptage",message="Votre texte décrypté est : %s " % dechiffrage_mots(txt))

def chiffrage_phrase():
    '''
    Ici on appelle la fonction de chiffrage de notre fichier Fonctions_chiffredecesar
    '''
    if verification_textarea_content():
        txt = get_textarea_content()
        showinfo(title="cryptage",message="Votre texte crypté est : %s " % chiffrage_mots(txt))

frame = Tk()

frame.title("Chiffre de César")
frame.minsize(1000, 600)

frame.rowconfigure(0, weight=1)
frame.rowconfigure(1, weight=1)
frame.columnconfigure(0, weight=1)
frame.columnconfigure(1, weight=1)


pos1="0.0"
textarea = Text(frame, wrap='word', bg=color_lightgrey) # création du composant
textarea.insert(pos1, "Entrez du texte") # insertion d'un texte
textarea.pack()

textarea.grid(row=0, columnspan=2, padx=120, pady=20, sticky='nesw')

Chiffrement_button=Button(frame, text="Chiffrer le texte", command=chiffrage_phrase, bg=color_grey)
Dechiffrement_button=Button(frame, text="Déchiffrer le texte", command=dechiffrage_phrase, bg=color_grey)

Chiffrement_button.grid(row=1, column=0)
Dechiffrement_button.grid(row=1, column=1)

frame.mainloop()

